Docker Build
 - docker-compose build
Docker Start
 - docker-compose up -d
Docker Down
 - docker-compose down

Run 
 - docker compose exec php-larajs bash
 - composer install
 - php artisan migrate
 - php artisan db:seed
 
 Frontend
 - npm install
 - npm start