<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Role::updateOrCreate([
            'name' =>  'ADMIN',
            'slug' => 'admin',
            'description' => null,
            'active' => '1'
        ]);
        Role::updateOrCreate([
            'name' =>  'PRODUCER',
            'slug' => 'producer',
            'description' => null,
            'active' => '1'
        ]);
        Role::updateOrCreate([
            'name' =>  'USER',
            'slug' => 'user',
            'description' => null,
            'active' => '1'
        ]);
    }
}