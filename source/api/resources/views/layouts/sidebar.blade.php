<div class="sidebar pe-4 pb-3">
    <nav class="navbar bg-secondary navbar-dark">
        <a href="{{ route('home') }}" class="navbar-brand mx-4 mb-3">
            <h3 class="text-primary"><i class="fas fa-yin-yang"></i> Larajs</h3>
        </a>
        <div class="d-flex align-items-center ms-4 mb-4">
            <div class="position-relative">
                <img class="rounded-circle" src="{{ Auth::user()->avatar ? Auth::user()->avatar : asset('img/avatar.jpg')}}" alt="avatar"
                    style="width: 40px; height: 40px;">
                <div
                    class="bg-success rounded-circle border border-2 border-white position-absolute end-0 bottom-0 p-1">
                </div>
            </div>
            <div class="ms-3">
                <h6 class="mb-0">{{ Auth::user()->first_name . Auth::user()->last_name }}</h6>
                <span>{{ Auth::user()->roles[0]->name }}</span>
            </div>
        </div>
        <div class="navbar-nav w-100">
            <a href="{{ route('home') }}" class="nav-item nav-link {{ (request()->is('/')) ? 'active' : '' }}"><i class="bi bi-house me-2"></i>Home</a>
            <div class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle {{ (request()->is('admin/users*')) ? 'active' : '' }}" data-bs-toggle="dropdown"><i class="bi bi-people me-2"></i>User</a>
                <div class="dropdown-menu bg-transparent border-0">
                    <a href="{{ route('users') }}" class="dropdown-item"><i class="bi bi-person"> user list </i></a>
                    <a href="{{ route('create_user') }}" class="dropdown-item"><i class="bi bi-plus-circle"> add user </i></a>
                </div>
            </div>
            <a href="widget.html" class="nav-item nav-link"><i class="fa fa-th me-2"></i>Widgets</a>
        </div>
    </nav>
</div>