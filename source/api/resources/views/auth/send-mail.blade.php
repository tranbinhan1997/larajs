@extends('auth.layout')

@section('content')
    <div class="content__send-email">
        <div class="send__email">
            <div class="send__email-title">
                I SENT AN E-MAIL
            </div>
            <div class="send__email-content">
                An email has been sent to the registered email address. Please access the URL of the email and complete the
                main
                registration procedure.
            </div>
        </div>
    </div>
@stop
