@extends('auth.layout')

@section('content')
    <div class="content">
        <div class="content__sns-registration">
            <div class="content__login-brand"><img class="registration_logo" src="{{ asset('images/logo.png')}}" alt="logo"></div>
            <a href="{{ route('signup') }}"><button class="btn btn-primary btn-signup">SIGN UP</button></a>
            <a href="{{ route('login') }}"><button class="btn btn-primary btn-login">LOG IN</button></a>
            <div class="content__login-or">OR</div>
            <div class="content__login-description">
                New registration. login with other services
            </div>
            <div class="content__login-icon">
               <a href="{{ route('auth_facebook') }}"><img class="icon login-facebook" src="{{ asset('images/facebook.svg')}}" alt="" /></a> 
               <a href="{{ route('auth_google') }}"><img class="icon login-weibo" src="{{ asset('images/google.svg')}}" alt="" /></a> 
            </div>
        </div>
    </div>
@stop
