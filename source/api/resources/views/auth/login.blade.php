@extends('auth.layout')

@section('content')
    @if (Session::has('error-login'))
        <div class="alert alert-danger">
            {{ Session::get('error-login') }}
        </div>
    @endif
    @if (Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
    @endif
    <div class="content__login">
        <div class="login-title">
            LOG IN
        </div>
        <form action="{{ route('login') }}" method="POST">
            @csrf
            <div class="form-group">
                <input type="email" name="email" class="form-control input-email" placeholder="E-mail">
                @if ($errors->has('email'))
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                @endif
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control input-password" placeholder="Password">
                @if ($errors->has('password'))
                    <span class="text-danger">{{ $errors->first('password') }}</span>
                @endif
            </div>
            <div class="forgot-password">
                <a href="{{ route('send_mail_reset_password') }}"><span>If you forgot your password</span></a>
            </div>
            <div class="subscript-btn-next">
                <button class="btn btn-subscription-next">NEXT</button>
            </div>
        </form>
    </div>
@stop
