@extends('auth.layout')

@section('content')
    <div class="content__signup">
        <div class="content__signup-main">
            <div class="content__signup-title">SIGN UP</div>
            <form action="{{ route('signup') }}" method="POST">
                @csrf
                <div class="content__signup-name">
                    <label for="first-name" class="first-name">
                        <input type="text" id="first-name" name="first_name" placeholder="First name"
                            class="form-control input-first-name" />
                        @if ($errors->has('first_name'))
                            <span class="text-danger">{{ $errors->first('first_name') }}</span>
                        @endif
                    </label>
                    <label for="last_name" class="last-name">
                        <input type="text" id="last_name" name="last_name" placeholder="Last name"
                            class="form-control input-last-name" />
                        @if ($errors->has('last_name'))
                            <span class="text-danger">{{ $errors->first('last_name') }}</span>
                        @endif
                    </label>
                </div>
                <div class="form-group">
                    <input type="email" name="email" placeholder="E-mail" class="form-control input-form">
                    @if ($errors->has('email'))
                        <span class="text-danger">{{ $errors->first('email') }}</span>
                    @endif
                </div>
                <div class="form-group">
                    <input type="password" name="password" placeholder="Password" class="form-control input-form">
                    @if ($errors->has('password'))
                        <span class="text-danger">{{ $errors->first('password') }}</span>
                    @endif
                </div>
                <div class="form-group">
                    <input type="password" name="password-confirmation" placeholder="Password confirmation"
                        class="form-control input-form">
                    @if ($errors->has('password-confirmation'))
                        <span class="text-danger">{{ $errors->first('password-confirmation') }}</span>
                    @endif
                </div>
                <div class="form-check">
                    <div class="round">
                        <input type="checkbox" name="checked-signup" id="checkbox" />
                        <label for="checkbox"></label>
                    </div>
                    <label class="check-title" for="checkbox">I agree to the <a href="">terms</a> of use</label>
                </div>
                <div class="form-group">
                    @if ($errors->has('checked-signup'))
                        <span class="text-danger">{{ $errors->first('checked-signup') }}</span>
                    @endif
                </div>
                <div class="subscript-btn-next">
                    <button class="btn btn-subscription-next">NEXT</button>
                </div>
            </form>
        </div>
    </div>
@stop
