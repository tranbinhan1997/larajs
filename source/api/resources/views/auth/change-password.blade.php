@extends('auth.layout')

@section('content')
<div class="content__change-password">
    <div class="change-password-title">
        CHANGE PASSWORD
    </div>
    <form action="{{ route('reset_password', $userUuid) }}" method="POST">
        @csrf
        <div class="form-group">
            <input type="password" name="new_password" class="form-control input-password" placeholder="New password">
            @if ($errors->has('new_password'))
                <span class="text-danger">{{ $errors->first('new_password') }}</span>
            @endif
        </div>
        <div class="form-group">
            <input type="password" name="password-confirmation" class="form-control input-password" placeholder="Password confirmation">
            @if ($errors->has('password-confirmation'))
                <span class="text-danger">{{ $errors->first('password-confirmation') }}</span>
            @endif
        </div>
        <div class="subscript-btn-next">
            <button class="btn btn-subscription-next">NEXT</button>
        </div>
    </form>
</div>
@stop
