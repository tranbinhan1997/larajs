@extends('auth.layout')

@section('content')
    <div class="content__reset-password">
        <div class="reset__password">
            <div class="reset__password-title">
                PASSWORD RESET
            </div>
            <form action="{{ route('send_mail_reset_password') }}" method="POST">
                @csrf
                <div class="form-group">
                    <input type="email" name="email" class="form-control input-email" placeholder="E-mail">
                    @if ($errors->has('email'))
                        <span class="text-danger">{{ $errors->first('email') }}</span>
                    @endif
                </div>
                <div class="subscript-btn-next">
                    <button class="btn btn-subscription-next">NEXT</button>
                </div>
            </form>
        </div>
    </div>
@stop
