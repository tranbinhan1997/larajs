<!DOCTYPE html>
<html>
<head>
    <title>Larajs</title>
</head>
<body>
    <?php if($mailData['code']): ?>
        Code: <?= $mailData['code'] ?>
    <?php else: ?>
        <a href="{{ route('reset_password', $mailData['user_uuid']) }}">Reset password</a>
    <?php endif ?>
</body>
</html>