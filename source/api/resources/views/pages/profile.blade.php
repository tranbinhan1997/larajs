@extends('layouts.app')

@section('content')
    @if (Session::has('error'))
        <div class="alert alert-danger">
            {{ Session::get('error') }}
        </div>
    @endif
    @if (Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
    @endif
    <div class="container-fluid pt-4 px-4">
        <div class="row g-4">
            <div class="col-sm-12 col-xl-6">
                <div class="bg-secondary rounded p-4 mb-4">
                    <h6 class="mb-4">Change E-mail</h6>
                    <form action="{{ route('send_mail_change_email') }}" method="POST">
                        @csrf
                        <div class="row mb-3">
                            <label for="email" class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-10">
                                <input type="email" value="{{ $user->email }}" name="email" class="form-control"
                                    id="email">
                                @if ($errors->has('email'))
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">NEXT</button>
                    </form>
                </div>
                <div class="bg-secondary rounded p-4">
                    <h6 class="mb-4">Change Password</h6>
                    <form action="{{ route('change_password') }}" method="POST">
                        @csrf
                        <div class="row mb-3">
                            <label for="current-password" class="col-sm-4 col-form-label">Current password</label>
                            <div class="col-sm-8">
                                <input type="password" autocomplete="off" name="current_password" class="form-control"
                                    id="current-password">
                                @if ($errors->has('current_password'))
                                    <span class="text-danger">{{ $errors->first('current_password') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="new-password" class="col-sm-4 col-form-label">New password</label>
                            <div class="col-sm-8">
                                <input type="password" autocomplete="off" name="new_password" class="form-control"
                                    id="new-password">
                                @if ($errors->has('new_password'))
                                    <span class="text-danger">{{ $errors->first('new_password') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="confirmation-password" class="col-sm-4 col-form-label">Confirmation password</label>
                            <div class="col-sm-8">
                                <input type="password" autocomplete="off" name="confirmation_password" class="form-control"
                                    id="confirmation-password">
                                @if ($errors->has('confirmation_password'))
                                    <span class="text-danger">{{ $errors->first('confirmation_password') }}</span>
                                @endif
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">NEXT</button>
                    </form>
                </div>
            </div>
            <div class="col-sm-12 col-xl-6">
                <div class="bg-secondary rounded p-4">
                    <h6 class="mb-4">Change Info</h6>
                    <form action="{{ route('change_info') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-3">
                            <label for="avatar" class="form-label">Avatar</label>
                            <input type="file" class="form-control" hidden onchange="document.getElementById('avatar').src = window.URL.createObjectURL(this.files[0])" name="avatar" id="avatar-input">
                            <div class="dropzone">
                                <label for="avatar-input" class="dz-message needsclick">
                                    <img class="avatar img-thumbnail"src="{{ $user->avatar ? $user->avatar : asset('img/avatar.jpg') }}"
                                       id="avatar" alt="avatar">
                                </label>
                            </div>
                            @if ($errors->has('avatar'))
                                <span class="text-danger">{{ $errors->first('avatar') }}</span>
                            @endif
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-6">
                                <label for="first-name" class="form-label">Firs name</label>
                                <input type="text" autocomplete="off" value="{{ $user->first_name }}"
                                    class="form-control" name="first_name" id="first-name">
                                @if ($errors->has('first_name'))
                                    <span class="text-danger">{{ $errors->first('first_name') }}</span>
                                @endif
                            </div>
                            <div class="col-sm-6">
                                <label for="last-name" class="form-label">Last name</label>
                                <input type="text" autocomplete="off" value="{{ $user->last_name }}"
                                    class="form-control" name="last_name" id="last-name">
                                @if ($errors->has('last_name'))
                                    <span class="text-danger">{{ $errors->first('last_name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="phone" class="form-label">Phone</label>
                            <input type="number" autocomplete="off" value="{{ $user->phone }}" class="form-control"
                                name="phone" id="phone">
                            @if ($errors->has('phone'))
                                <span class="text-danger">{{ $errors->first('phone') }}</span>
                            @endif
                        </div>
                        <div class="mb-3">
                            <label for="birthday" class="form-label">Birthday</label>
                            <input type="text" autocomplete="off" class="form-control" value="{{ $user->birthday }}"
                                name="birthday" id="birthday">
                            @if ($errors->has('birthday'))
                                <span class="text-danger">{{ $errors->first('birthday') }}</span>
                            @endif
                        </div>
                        <div class="mb-3">
                            <label for="last-name" class="form-label">Gender</label>
                            <div class="d-flex">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="gender" id="man"
                                        value="1" {{ $user->gender == 1 ? 'checked' : '' }}>
                                    <label class="form-check-label" for="man">
                                        Man
                                    </label>
                                </div>
                                <div class="form-check mx-3">
                                    <input class="form-check-input" type="radio" name="gender" id="woman"
                                        value="2" {{ $user->gender == 2 ? 'checked' : '' }}>
                                    <label class="form-check-label" for="woman">
                                        Woman
                                    </label>
                                </div>
                            </div>
                            @if ($errors->has('gender'))
                                <span class="text-danger">{{ $errors->first('gender') }}</span>
                            @endif
                        </div>
                        <div class="mb-3">
                            <label for="address" class="form-label">Address</label>
                            <textarea class="form-control" autocomplete="off" name="address" id="address">{{ $user->address }}</textarea>
                            @if ($errors->has('address'))
                                <span class="text-danger">{{ $errors->first('address') }}</span>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary">NEXT</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        let ProfileModule = {
            init() {
                this.initFormatDate();
            },

            initFormatDate() {
                $("#birthday").datepicker({
                    dateFormat: 'yy-mm-dd',
                    onClose: function(dateText, inst) {
                        $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, inst
                            .selectedDay, 1));
                    }
                });
            }
        }

        $(function() {
            ProfileModule.init();
        });
    </script>
@stop
