@extends('layouts.app')

@section('content')
    @if (Session::has('error'))
        <div class="alert alert-danger">
            {{ Session::get('error') }}
        </div>
    @endif
    @if (Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
    @endif
    <div class="container-fluid pt-4 px-4">
        <div class="row bg-secondary rounded p-4">
            <div class="col-sm-3">
                <img class="user-avatar-detail img-thumbnail" src="{{ $user->avatar }}" alt="avatar">
                <h6 class="user-name-detail">{{ $user->first_name .' '.$user->last_name }}</h6>
            </div>
            <div class="col-sm-9">
                <div class="row">
                    <div class="col-sm-3">Email:</div>
                    <div class="col-sm-9">{{ $user->email }}</div>
                </div>
                <div class="row pt-2">
                    <div class="col-sm-3">Phone:</div>
                    <div class="col-sm-9">{{ $user->phone }}</div>
                </div>
                <div class="row pt-2">
                    <div class="col-sm-3">Birthday:</div>
                    <div class="col-sm-9">{{ $user->birthday }}</div>
                </div>
                <div class="row pt-2">
                    <div class="col-sm-3">Gender:</div>
                    <div class="col-sm-9">{{ $user->gender == 1 ? 'Men' : 'Women' }}</div>
                </div>
                <div class="row pt-2">
                    <div class="col-sm-3">Address:</div>
                    <div class="col-sm-9">{{ $user->address }}</div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        let UserDetailModule = {
            init() {
            },
        }

        $(function() {
            UserDetailModule.init();
        });
    </script>
@stop
