@extends('layouts.app')

@section('content')
    @if (Session::has('error'))
        <div class="alert alert-danger">
            {{ Session::get('error') }}
        </div>
    @endif
    @if (Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
    @endif
    <div class="container-fluid pt-4 px-4">
        <div class="col-sm-12 bg-secondary rounded p-4">
            <div class="col-sm-6">
                <h6 class="mb-4">Create User</h6>
                <form action="{{ route('create_user') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="mb-3">
                        <label for="avatar" class="form-label">Avatar</label>
                        <input type="file" class="form-control" hidden
                            onchange="document.getElementById('avatar').src = window.URL.createObjectURL(this.files[0])"
                            name="avatar" id="avatar-input">
                        <div class="dropzone">
                            <label for="avatar-input" class="dz-message needsclick">
                                <img class="avatar img-thumbnail"src="{{ asset('img/avatar.jpg') }}" id="avatar"
                                    alt="avatar">
                            </label>
                        </div>
                        @if ($errors->has('avatar'))
                            <span class="text-danger">{{ $errors->first('avatar') }}</span>
                        @endif
                    </div>
                    <div class="row mb-3">
                        <div class="col-sm">
                            <label for="first-name" class="form-label">Firs name</label>
                            <input type="text" autocomplete="off" value="" class="form-control" name="first_name"
                                id="first-name">
                            @if ($errors->has('first_name'))
                                <span class="text-danger">{{ $errors->first('first_name') }}</span>
                            @endif
                        </div>
                        <div class="col-sm">
                            <label for="last-name" class="form-label">Last name</label>
                            <input type="text" autocomplete="off" value="" class="form-control" name="last_name"
                                id="last-name">
                            @if ($errors->has('last_name'))
                                <span class="text-danger">{{ $errors->first('last_name') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="email" class="form-label">Email</label>
                        <input type="email" autocomplete="off" value="" class="form-control" name="email"
                            id="email">
                        @if ($errors->has('email'))
                            <span class="text-danger">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                    <div class="mb-3">
                        <label for="phone" class="form-label">Phone</label>
                        <input type="number" autocomplete="off" value="" class="form-control" name="phone"
                            id="phone">
                        @if ($errors->has('phone'))
                            <span class="text-danger">{{ $errors->first('phone') }}</span>
                        @endif
                    </div>
                    <div class="mb-3">
                        <label for="birthday" class="form-label">Birthday</label>
                        <input type="text" autocomplete="off" class="form-control" value="" name="birthday"
                            id="birthday">
                        @if ($errors->has('birthday'))
                            <span class="text-danger">{{ $errors->first('birthday') }}</span>
                        @endif
                    </div>
                    <div class="mb-3">
                        <label for="last-name" class="form-label">Gender</label>
                        <div class="d-flex">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="gender" id="man" value="1">
                                <label class="form-check-label" for="man">
                                    Man
                                </label>
                            </div>
                            <div class="form-check mx-3">
                                <input class="form-check-input" type="radio" name="gender" id="woman" value="2">
                                <label class="form-check-label" for="woman">
                                    Woman
                                </label>
                            </div>
                        </div>
                        @if ($errors->has('gender'))
                            <span class="text-danger">{{ $errors->first('gender') }}</span>
                        @endif
                    </div>
                    <div class="mb-3">
                        <label for="address" class="form-label">Address</label>
                        <textarea class="form-control" autocomplete="off" name="address" id="address"></textarea>
                        @if ($errors->has('address'))
                            <span class="text-danger">{{ $errors->first('address') }}</span>
                        @endif
                    </div>
                    <div class="mb-3">
                        <label for="password" class="form-label">Password</label>
                        <input type="password" autocomplete="off" value="" class="form-control" name="password"
                            id="password">
                        @if ($errors->has('password'))
                            <span class="text-danger">{{ $errors->first('password') }}</span>
                        @endif
                    </div>
                    <div class="mb-3">
                        <label for="password-confirmation" class="form-label">Confirmation password</label>
                        <input type="password" autocomplete="off" value="" class="form-control" name="password_confirmation"
                        id="password-confirmation">
                        @if ($errors->has('password_confirmation'))
                            <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-primary">NEXT</button>
                </form>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        let UserCreateModule = {
            init() {
                this.initFormatDate();
            },

            initFormatDate() {
                $("#birthday").datepicker({
                    dateFormat: 'yy-mm-dd',
                    onClose: function(dateText, inst) {
                        $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, inst
                            .selectedDay, 1));
                    }
                });
            },

        }

        $(function() {
            UserCreateModule.init();
        });
    </script>
@stop
