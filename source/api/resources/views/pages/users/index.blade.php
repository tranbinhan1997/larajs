@extends('layouts.app')

@section('content')
    @if (Session::has('error'))
        <div class="alert alert-danger">
            {{ Session::get('error') }}
        </div>
    @endif
    @if (Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
    @endif
    <div class="container-fluid pt-4 px-4">
        <div class="bg-secondary rounded p-4">
            <div class="d-flex align-items-center justify-content-between mb-4">
                <h6 class="mb-0">Search <i class="bi bi-search"></i></h6>
            </div>
            <form>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="mb-3">
                            <label class="form-label">Email</label>
                            <input type="text" autocomplete="off" placeholder="email" class="form-control"
                                id="filter-email" name="email">
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Phone</label>
                            <input type="number" autocomplete="off" placeholder="phone" class="form-control"
                                id="filter-phone" name="phone">
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Gender</label>
                            <select class="form-select" id="filter-gender">
                                <option value="" selected>All</option>
                                <option value="1">Men</option>
                                <option value="2">Women</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="row mb-3 align-items-center">
                            <label class="form-label">Name</label>
                            <div class="col-sm">
                                <input type="text" autocomplete="off" placeholder="first name" class="form-control"
                                    id="filter-first-name" name="first_name">
                            </div>
                            ~
                            <div class="col-sm">
                                <input type="text" autocomplete="off" placeholder="last name" class="form-control"
                                    id="filter-last-name" name="last_name">
                            </div>
                        </div>
                        <div class="row mb-3 align-items-center">
                            <label class="form-label">Birthday</label>
                            <div class="col-sm">
                                <input type="text" autocomplete="off" placeholder="from date"
                                    class="form-control date-time" id="filter-from-date" name="start_date">
                            </div>
                            ~
                            <div class="col-sm">
                                <input type="text" autocomplete="off" placeholder="to date"
                                    class="form-control date-time" id="filter-end-date" name="end_date">
                            </div>
                        </div>
                        <div class="mb-3 col-sm-4">
                            <label class="form-label">Role</label>
                            <select class="form-select" id="filter-role">
                                <option value="" selected>All</option>
                                <option value="1">Admin</option>
                                <option value="2">Producer</option>
                                <option value="3">User</option>
                            </select>
                        </div>
                    </div>
                </div>
            </form>
            <button class="btn btn-light btn-clear"><i class="bi bi-trash2"> Clear</i></button>
        </div>
    </div>
    <div class="container-fluid pt-4 px-4">
        <div class="bg-secondary text-center rounded p-4">
            <div class="d-flex align-items-center justify-content-between mb-4">
                <h6 class="mb-0">USER LIST</h6>
                <div>
                    <a href="{{ route('create_user') }}" class="btn btn-primary"><i class="bi bi-plus-square"> Create</i></a>
                    <a href="{{ route('export_user_pdf') }}" target="_blank" class="btn btn-success"><i class="bi bi-plus-square"> Export PDF</i></a>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table text-start align-middle table-bordered table-hover mb-0" id="data-table">
                    <thead>
                        <tr class="text-white">
                            <th scope="col">#ID</th>
                            <th scope="col">First name</th>
                            <th scope="col">Last name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Gender</th>
                            <th scope="col">Birthday</th>
                            <th scope="col">Roles</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="confirm" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Do you want to active user?</h5>
            </div>
            <div class="modal-footer">
                <form action="{{ route('confirm_user') }}" method="post">
                    @csrf
                    <input type="hidden" class="user-id-confirm" name="user_id">
                    <button type="submit" class="btn btn-primary">Next</button>
                </form>
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="delete" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Do you want to delete user?</h5>
            </div>
            <div class="modal-footer">
                <form action="{{ route('delete_user') }}" method="post">
                    @csrf
                    <input type="hidden" class="user-id-confirm" name="user_id">
                    <button type="submit" class="btn btn-primary">Next</button>
                </form>
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

@stop

@section('javascript')
    <script>
        let UserModule = {
            init() {
                this.initFormatDate();
                this.dataTable();
            },

            initFormatDate() {
                $(".date-time").datepicker({
                    dateFormat: 'yy-mm-dd',
                    onClose: function(dateText, inst) {
                        $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, inst
                            .selectedDay, 1));
                    }
                });
            },

            dataTable() {
                var table = $('#data-table').DataTable({
                    processing: true,
                    serverSide: true,
                    searching: false,
                    stateSave: true,
                    order: [[ 0, "desc" ]],
                    ajax: {
                        url: "{{ route('users') }}",
                        data: function(d) {
                            d.email = $('#filter-email').val(),
                            d.phone = $('#filter-phone').val(),
                            d.gender = $('#filter-gender').val(),
                            d.first_name = $('#filter-first-name').val(),
                            d.last_name = $('#filter-last-name').val(),
                            d.from_date = $('#filter-from-date').val(),
                            d.end_date = $('#filter-end-date').val(),
                            d.role = $('#filter-role').val()
                        }
                    },
                    columns: [{
                            data: 'id',
                            render: function(data, type, row) {
                                if (type === 'display') {
                                    return `<a href="/admin/user/${row.user_uuid}">${data}</a>`;
                                }
                                return data;
                            }
                        },
                        {
                            data: 'first_name',
                            render: function(data, type, row) {
                                if (type === 'display') {
                                    return data;
                                }
                                return data;
                            }
                        },
                        {
                            data: 'last_name',
                            render: function(data, type, row) {
                                if (type === 'display') {
                                    return data;
                                }
                                return data;
                            }
                        },
                        {
                            data: 'email',
                            render: function(data, type, row) {
                                if (type === 'display') {
                                    return data;
                                }
                                return data;
                            }
                        },
                        {
                            data: 'phone',
                            render: function(data, type, row) {
                                if (type === 'display') {
                                    return data;
                                }
                                return data;
                            }
                        },
                        {
                            data: 'gender',
                            render: function(data, type, row) {
                                if (type === 'display') {
                                    return data == 1 ? 'men' : 'women';
                                }
                                return data;
                            }
                        },
                        {
                            data: 'birthday',
                            render: function(data, type, row) {
                                if (type === 'display') {
                                    return data;
                                }
                                return data;
                            }
                        },
                        {
                            data: 'roles',
                            render: function(data, type, row) {
                                if (type === 'display') {
                                    return data;
                                }
                                return data;
                            }
                        },
                        {
                            data: 'user_uuid',
                            render: function(data, type, row) {
                                if (type === 'display' && data) {
                                    let btnActive = '';
                                    if(row.active == 1) {
                                        btnActive = `<button class="btn btn-sm btn-success" onClick="UserModule.getUserId(${row.id})" data-bs-toggle="modal" data-bs-target="#confirm"><i class="bi bi-check-circle"></i></button>`;
                                    } else {
                                        btnActive = `<button class="btn btn-sm btn-light" onClick="UserModule.getUserId(${row.id})" data-bs-toggle="modal" data-bs-target="#confirm"><i class="bi bi-check-circle"></i></button>`;
                                    }
                                    return `${btnActive}
                                            <a class="btn btn-sm btn-warning" href="/admin/users/update/${row.id}"><i class="bi bi-pencil"></i></a>
                                            <button class="btn btn-sm btn-danger" onClick="UserModule.getUserId(${row.id})" data-bs-toggle="modal" data-bs-target="#delete"><i class="bi bi-trash"></i></button>`;
                                }
                                return '-'
                            }
                        },
                    ]
                });

                for (let filter of ["#filter-email", "#filter-phone", "#filter-first-name", "#filter-last-name"]) {
                    $(`${filter}`).keyup(function() {
                        table.draw();
                    });
                }
                for (let filter of ["#filter-gender", "#filter-from-date", "#filter-end-date", "#filter-role"]) {
                    $(`${filter}`).change(function() {
                        table.draw();
                    });
                }

                $('.btn-clear').click(function(e) {
                    for (let field of ["#filter-email", "#filter-phone", "#filter-first-name", "#filter-last-name", "#filter-gender", "#filter-from-date", "#filter-end-date", "#filter-role"]) {
                        $(field).val('');
                        table.draw();
                    }
                });
            }, 

            getUserId(id) {
                $('.user-id-confirm').val(id);
            }
        }

        $(function() {
            UserModule.init();
        });
    </script>
@stop
