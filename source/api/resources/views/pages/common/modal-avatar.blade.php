<?php
/**
 * @param null|string $image_urls
 */
$image_urls = isset($image_urls) ? json_decode($image_urls, true) : null;
?>
<div class="modal fade modal-upload-images unioss-up-modal" id="modal-upload-images" tabindex="-1" aria-labelledby="modal-upload-images" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered unioss-up-modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title-hear">
                    <button type="button" class="close button-upload-cancel" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h5 class="modal-title fw-bold">商品画像アップロード</h5>
                </div>
            </div>
            <div class="modal-body bg-light">
            <div class="file-number-description text-center pb-2">5件までアップロードできます。 <br> 現状、アップロードした件数 : <span id="number-file-image"></span> / 5</div>
                <div class="dz-message dz-default dropzone-message cursor-pointer" data-dz-message>クリックしてファイルを参照する<br/>又はドラッグ&ドロップします</div>
                <div class="table mb-0" id="previews-image">
                    <div id="template-image" class="panel dz-image-preview">
                        <div class="panel-body dropzone-item">
                            <div class="preview p-2">
                                <img data-dz-thumbnail class="dropzone-thumbnail" src="" alt="画像" />
                                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                                    <div class="progress-bar progress-bar-success d-none" data-dz-uploadprogress>PROGRESS</div>
                                </div>
                            </div>
                            <div class="dropzone-item-detail">
                                <div class="dz-filename"><span data-dz-name></span></div>
                                <div class="dz-size" data-dz-size></div>
                            </div>
                            <button data-dz-remove class="btn btn-outline-light border-0 text-dark delete bg-white">
                                <span class="mdi mdi-window-close"></span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-between">
                <button type="button" class="btn btn-primary px-5 button-upload-image mb-auto">アップロード</button>
                <button type="button" class="btn btn-default px-5 button-upload-cancel" data-dismiss="modal" aria-label="Close">キャンセル</button>
            </div>
        </div>
    </div>
</div>


<script>
    let ModalUploadImage = {
        uploadMultiple: true, // Default upload multi images
        url: ``,
        init() {
            this.initDropzone();
        },

        initDropzone() {
            let me = this;
            let previewNode = document.querySelector("#template-image");
            previewNode.id = "";
            let previewTemplate = previewNode.parentNode.innerHTML;
            previewNode.parentNode.removeChild(previewNode);
            
            let maxFiles = 5; // Upload max files is 5
            
            let buttonUploads = $('.button-upload-image');
            buttonUploads.attr('disabled', true);

            let numberFile = $('#number-file-image');

            let dropzoneImage = $('#dropzone-upload-images').dropzone({
                // The configuration we've talked about above
                url: me.url,
                autoProcessQueue: false,
                maxFilesize: MAX_FILES_SIZE,
                maxRequestSize: 128,
                maxThumbnailFilesize: MAX_THUMBNAIL_FILES_SIZE,
                thumbnailHeight: THUMBNAIL_HEIGHT,
                thumbnailWidth: THUMBNAIL_WIDTH,
                uploadMultiple: me.uploadMultiple,
                parallelUploads: maxFiles,
                maxFiles: maxFiles,
                previewTemplate: previewTemplate,
                previewsContainer: "#previews-image",
                acceptFiles: 'image/jpeg,image/jpg,image/png',
                acceptedMimeTypes: 'image/jpeg,image/jpg,image/png',

                // The setting up of the dropzone
                init: function () {
                    
                    let myDropzone = this;

                    let sendingMethod = 'sending';
                    let successMethod = 'success';
                    let storeImagesJson = [];
                    let imagesS3Id = [];
                    if('<?= !empty($image_urls) ?>') {
                        storeImagesJson = <?= json_encode($image_urls) ?>;
                    }
                    if (me.uploadMultiple === true) {
                        sendingMethod = 'sendingmultiple';
                        successMethod = 'successmultiple';
                    } else {
                        $('.modal-upload-images .file-number-description').remove()
                    }
                    
                    numberFile.text(0);
                    let number = 0;

                    if('<?= !empty($image_urls) ?>') {
                        me.pushImageToDropzone(myDropzone, storeImagesJson);
                        numberFile.text(storeImagesJson.length);

                        imagesS3Id = storeImagesJson.map((image) => { return image.s3_id; });
                        $("input[name=image_urls]").val(imagesS3Id.join(","));
                    }

                    this.on('addedfile', function(file) {
                        if (this.files.length > this.options.maxFiles) {
                            if (me.uploadMultiple === true) {
                                this.removeFile(file);
                                alertError(`ファイル数は${this.options.maxFiles}件以下にしてください。`);
                            } else {
                                this.removeFile(this.files[0]);
                                return true;
                            }
                        }
                        numberFile.text(this.files.length);
                        let totalSize = 0;
                        buttonUploads.attr('disabled', false)
                    }).on('error', function(file) {
                        switch (true) {
                            case !this.options.acceptFiles.split(",").includes(file.type):
                                this.removeFile(file);
                                alertError('ファイル形式が不正です。.jpeg、png形式のファイルを選択してください。');
                                break;
                            case file.size > this.options.maxFilesize * 1048576:
                                this.removeFile(file);
                                alertError(`ファイルサイズは${this.options.maxFilesize}MB以下にしてください。`);
                                break;
                        }
                    }).on('removedfile', function(file) {
                        buttonUploads.attr('disabled', false);

                        if (this.files.length === 0) {
                            buttonUploads.attr('disabled', false)
                        }
                        imagesS3Id = imagesS3Id.filter(id => id !== file.s3_id);
                        
                        numberFile.text(this.files.length);
                    }).on(sendingMethod , function(file, xhr, formData) {
                    }).on("processing", function (file) {
                    }).on(successMethod, function (files, response) {
                        // Gets triggered when the files have successfully been sent.
                        // Redirect user or notify of success.
                        this.removeAllFiles();
                        hideLoading();
                        if (response.status == RESPONSE_SUCCESS) {
                            $('#modal-upload-images').modal('hide');
                        }
                        alert(response.message)
                    });

                    // First change the button to actually tell Dropzone to process the queue.
                    buttonUploads.click(function (e) {
                        e.preventDefault();
                        e.stopPropagation();
                        $('#modal-upload-images').modal('hide');

                        // Reset list of Store Image when delete image
                        let images_uploaded_s3 = [];
                        $(`#input-images img`).attr('src', '').addClass(`d-none`);
                        $(`#input-images input[type=file]`).val('');
                        $("input[name=image_urls]").val(imagesS3Id.join(","));
                        storeImagesJson = [];

                        myDropzone.files.map((item, idx) => {
                            if($(`#image${idx}_point_purchase_product`).hasClass(`d-none`)) {
                                $(`#image${idx}_point_purchase_product`).removeClass(`d-none`);
                                $(`.upload-image[data-index="${idx}"]`).removeClass(`d-none`);
                            }

                            storeImagesJson = [...myDropzone.files];
                            if(item.s3_id === undefined) {// item.s3_id is null that item not upload in s3
                                // prepare template
                                let storeImage = $(`.upload-image[data-index="${idx}"]`);
                                if (storeImage.length === 0) {
                                    storeImage = $(me.store_image_template);
                                    storeImage.attr('data-index', idx)
                                }
                                let image_file = storeImage.find('input[type="file"]');
                                let image_element = storeImage.find('img');

                                // attach data attribute
                                image_file.attr('name',`tmp_image${idx}_point_purchase_product`).attr('id', `tmp_image${idx}_point_purchase_product`);
                                image_element.attr('src', myDropzone.files[idx].dataURL).attr('id', `image${idx}_point_purchase_product`)

                                $('.button-upload-images').before(storeImage)

                                // attach file to form input
                                let blob = me.dataURLtoBlob(myDropzone.files[idx].dataURL);
                                let file = new File([blob], myDropzone.files[idx].name, {type: myDropzone.files[idx].type});
                                let container = new DataTransfer();
                                container.items.add(file);
                                image_file.get(0).files = container.files;
                            } else {
                                let storeImage = $(`.upload-image[data-index="${idx}"]`);
                                let image_element = storeImage.find('img');

                                image_element.attr('src', item.url);
                                // Add image src after reset list of store image
                                images_uploaded_s3.push(item.s3_id);
                                $("input[name=image_urls]").val(images_uploaded_s3.join(','));
                            }
                        });

                        $(`#input-images img[src=""]`).closest('.upload-image').remove();

                        if (myDropzone.files.length >= myDropzone.options.maxFiles) {
                            $('.button-upload-images').hide();
                        } else {
                            $('.button-upload-images').show();
                        }
                    })

                    if (this.files.length >= this.options.maxFiles) {
                        $('.button-upload-images').hide();
                    } else {
                        $('.button-upload-images').show();
                    }

                    $(`#modal-upload-images .button-upload-cancel`).on('click', function(){
                        myDropzone.removeAllFiles(true);

                        if(storeImagesJson.length > 0) {
                            me.pushImageToDropzone(myDropzone, storeImagesJson);
                            numberFile.text(storeImagesJson.length);

                            me.setValueImagesInput('image_urls', storeImagesJson.map((image) => { return image.s3_id; }));
                        }
                    });
                }
            })

            $(document).on('click', '#input-images .upload-image', function () {
                $('#images-modal').click()
            })
        },

        dataURLtoBlob(dataurl) {
            var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
                bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
            while(n--){
                u8arr[n] = bstr.charCodeAt(n);
            }
            return new Blob([u8arr], {type:mime});
        },

        pushImageToDropzone(dropZone, imagesJSON){
            imagesJSON.map((file, idx) => {
                if(file.s3_id) { // images is uploaded on S3 before
                    file = {
                        name: file.name,
                        size: file.size,
                        url: file.url,
                        s3_id: file.s3_id,
                        status: Dropzone.ADDED,
                        accepted: true
                    };

                    dropZone.emit("addedfile", file);
                    dropZone.emit("success", file);
                    dropZone.emit("thumbnail", file, file.url);
                    dropZone.files.push(file);
                } else {
                    dropZone.addFile(file);
                }
            });
        },

        setValueImagesInput(inputName, valueInput = []) {
            $(`input[name=${inputName}]`).val(valueInput.join(','));
        },

        store_image_template: `<div class="upload-image" data-index="">
            <input type="file" class="d-none" accept="image/gif,image/jpeg,image/png"/>
            <img alt="画像" class="point-purchase-product-images"/>
        </div>`,

    };
    Dropzone.autoDiscover = false;
    ModalUploadImage.init();

</script>