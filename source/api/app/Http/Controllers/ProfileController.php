<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Mail\ChangeEmailMail;
use Mail;
use App\Models\User;
use Hash;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:ADMIN,PRODUCER,USER');
    }

    public function index()
    {
        $user = Auth::user();
        return view('pages.profile', compact('user'));
    }

    public function sendMailchangeEmail(Request $request)
    {
        if ($request->isMethod('post')) {
            $request->validate([
                'email' => 'required|email|unique:users',
            ]);

            $user = Auth::user();

            $mailData = [
                'user_uuid' => $user->user_uuid,
                'email' => $request->email
            ];

            Mail::to($request->email)->send(new ChangeEmailMail($mailData));
            return redirect('profile');
        }
    }

    public function changeEmail(Request $request)
    {
        $userUuid = $request->segment(3);
        $email = $request->segment(2);
        $user = User::where('user_uuid', $userUuid)->update([
            'email' => $email
        ]);
        if($user) {
            return redirect('logout');
        } else {
            return back()->withInput();
        }
    }

    public function changePassword(Request $request)
    {
        if ($request->isMethod('post')) {
            $request->validate([
                'current_password' => 'required',
                'new_password' => 'required|min:6',
                'confirmation_password' => 'required|same:new_password',
            ]);

            if(!Hash::check($request->current_password, auth()->user()->password)){
                return back()->with("error", "Current Password Doesn't match!");
            }

            $user = User::whereId(auth()->user()->id)->update([
                'password' => Hash::make($request->new_password)
            ]);

            if($user) {
                return redirect('profile')->with("success", "Password editing successfully!");
            } else {
                return back()->with("error", "Password editing failed!");
            }
        }
    }

    public function changeInfo(Request $request)
    {
        $request->validate([
            'first_name' => 'max:255',
            'last_name' => 'max:255',
            'address' => 'max:255',
            'birthday' => 'date',
            'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $user = User::whereId(auth()->user()->id)->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone' => $request->phone,
            'address' => $request->address,
            'birthday' => $request->birthday,
            'gender' => $request->gender,
        ]);

        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $name = time() .'_'. $file->getClientOriginalName();
            $filePath = 'avatar/' . $name;
            $path = Storage::disk('s3')->put($filePath, file_get_contents($file));
            $path = Storage::disk('s3')->url( $filePath);
            $user = User::whereId(auth()->user()->id)->update([
                'avatar' => $path
            ]);
        }

        if($user) {
            return redirect('profile')->with("success", "Change successfully!");
        } else {
            return back()->with("error", "Change failed!");
        }
    }
}