<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Mail\SignupMail;
use App\Mail\ResetPasswordMail;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Session;
use Mail;
use Illuminate\Support\Facades\Crypt;
use Socialite;
use Exception;
use Illuminate\Support\Str;



class AuthController extends Controller
{

    public function registration()
    {
        return view('auth.registration');
    }

    public function index()
    {
    }

    public function login(Request $request)
    {
        if ($request->isMethod('post')) {
            $request->validate([
                'email' => 'required',
                'password' => 'required',
            ]);
        
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'active' => 1 ])) {
                return redirect()->route('home');
            }
            return redirect()->route('login')->with('error-login','Login details are not valid');
        }
        
        return view('auth.login');
    }

    public function signup(Request $request)
    {
        if ($request->isMethod('post')) {
            $request->validate([
                'first_name' => 'required|string|between:2,100',
                'last_name' => 'required|string|between:2,100',
                'email' => 'required|email|unique:users',
                'password' => 'required|min:6',
                'password-confirmation' => 'required|same:password',
                'checked-signup' => 'required'
            ]);
    
            DB::beginTransaction();
    
            try {
                $user = User::create([
                    'user_uuid' => Str::uuid()->toString(),
                    'first_name' => trim($request->first_name),
                    'last_name' => trim($request->last_name),
                    'email' => trim($request->email),
                    'password' => Hash::make(trim($request->password)),
                ]);
    
                $mailData = [
                    'user_uuid' => $user->user_uuid
                ];
    
                Mail::to($request->email)->send(new SignupMail($mailData));
                DB::commit();
                return redirect()->route('send_mail');
            } catch(Exception $e) {
                DB::rollBack();
                throw new Exception($e->getMessage());
            }   
           
            return back()->withInput();
        }

        return view('auth.signup');
    }

    public function sendMail()
    {
        return view('auth.send-mail');
    }

    public function verify($userUuid)
    {
        $user = User::where('user_uuid', $userUuid);

        if ($user->count() > 0) {
            $user->update([
                'active' => 2,
                'email_verified_at' => now(),
            ]);
            $notificationMessage = 'Email confirmation successful';
        } else {
            $notificationMessage ='Email confirmation failed';
        }

        return redirect()->route('login')->with('status', $notificationMessage);
    }

    public function sendMailResetPassword(Request $request)
    {
        if ($request->isMethod('post')) {
            $user = User::where('email', $request->email)->first();
            if($user) {
                $mailData = [
                    'user_uuid' => $user->user_uuid
                ];
                Mail::to($request->email)->send(new ResetPasswordMail($mailData));
                return redirect()->route('send_mail');
            } else {
                dd("error");
            }
        }

        return view('auth.reset-password');
    }

    public function resetPassword(Request $request)
    {
        $userUuid = $request->segment(3);
        if ($request->isMethod('post')) {
            $request->validate([
                'new_password' => 'required|min:6',
                'password-confirmation' => 'required|same:new_password',
            ]);
            $user = User::where('user_uuid', $userUuid)->update([
                'password' => Hash::make($request->new_password)
            ]);
            if($user) {
                return redirect()->route('login');
            } else {
                return back()->withInput();
            } 
        }

        return view('auth.change-password', compact('userUuid'));
    }

    public function redirectToFB()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleCallbackFacebook()
    {
        try {
     
            $user = Socialite::driver('facebook')->user();
      
            $finduser = User::where('social_id', $user->id)->first();
      
            if($finduser){
                Auth::login($finduser);
                return redirect()->route('home');
            }else{
                $newUser = User::create([
                    'first_name' => $user->name,
                    'email' => $user->email,
                    'social_id'=> $user->id,
                    'social_type'=> 'facebook',
                    'password' => encrypt('my-facebook'),
                    'active' => 1
                ]);
     
                Auth::login($newUser);
      
                return redirect()->route('home');
            }
     
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }

    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleCallbackGoogle()
    {
        try {
     
            $user = Socialite::driver('google')->user();
      
            $finduser = User::where('social_id', $user->id)->first();
      
            if($finduser){
                Auth::login($finduser);
                return redirect()->route('home');
      
            }else{
                $newUser = User::create([
                    'first_name' => $user->name,
                    'email' => $user->email,
                    'social_id'=> $user->id,
                    'social_type'=> 'google',
                    'password' => encrypt('my-google'),
                    'active' => 1
                ]);
     
                Auth::login($newUser);
      
                return redirect()->route('home');
            }
     
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }
 
    public function logout() {
        Session::flush();
        Auth::logout();
        return redirect()->route('registration');
    }
}
