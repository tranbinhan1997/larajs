<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Mail;
use App\Mail\ResetPasswordMail;
use Carbon\Carbon;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register', 'sendMailResetPassword', 'mailVerifiedCode', 'changePassWord']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        if (!$token = auth('api')->attempt($validator->validated())) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $this->createNewToken($token);
    }

    /**
     * Register a User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|between:2,100',
            'last_name' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        $user = User::create(array_merge(
            $validator->validated(),
            [
                'user_uuid' => Str::uuid()->toString(),
                'password' => Hash::make($request->password)
            ]
        ));

        return response()->json([
            'message' => 'User successfully registered',
            'user' => $user
        ], 201);
    }


    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth('api')->logout();

        return response()->json(['message' => 'User successfully signed out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->createNewToken(auth('api')->refresh());
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile()
    {
        return response()->json(auth('api')->user());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'user' => auth('api')->user()
        ]);
    }

    public function sendMailResetPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        $randCode = random_int(100000, 999999);

        $user = User::where('email', $request->email)->update([
            'email_verified_at' => Carbon::now()->addMinutes(1),
            'email_verified_code' => $randCode
        ]);

        if ($user) {
            $mailData = [
                'code' => $randCode
            ];
            Mail::to($request->email)->send(new ResetPasswordMail($mailData));
            return response()->json([
                'status' => 1,
                'randCode' => $randCode
            ], 201);
        } else {
            return response()->json([
                'message' => 'Invalid email',
            ], 400);
        }
    }

    public function mailVerifiedCode(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        $currentTime = Carbon::now();

        $user = User::where(['email' => $request->email, 'email_verified_code' => $request->code])->first();

        if ($user) {
            if ($currentTime < $user->email_verified_at) {
                $user->where('id', $user->id)->update([
                    'email_verified_code' => null
                ]);
                $status = 1;
                $messge = "success";
                $httpRequest = 201;
            } else {
                $status = 0;
                $messge = "The code has expired, please re-enter your email";
                $httpRequest = 400;
            }
        } else {
            $status = 0;
            $messge = "Invalid code";
            $httpRequest = 400;
        }

        return response()->json([
            'status' => $status,
            'message' => $messge,
        ], $httpRequest);
    }

    public function changePassWord(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'new_password' => 'required|string|min:6',
            'password_confirm' => 'required|same:new_password',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        $user = User::where('email', $request->email)->update([
            'password' => Hash::make($request->new_password)
        ]);

        if($user) {
            return response()->json([
                'status' => 1,
                'message' => 'User successfully changed password',
            ], 201);
        } else {
            return response()->json([
                'status' => 0,
                'message' => 'User changed password failed',
            ], 400);
        }


        
    }
}
