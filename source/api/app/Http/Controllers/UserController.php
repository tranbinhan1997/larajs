<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use DataTables;
use Illuminate\Support\Str; 
use Hash;
use Illuminate\Support\Facades\Storage;
use PDF;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:ADMIN');
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
              
            $data = User::select('*')->where('delete_flg' , 0);
   
            return Datatables::of($data)    
                    ->addIndexColumn()
                    ->filter(function ($instance) use ($request) {
                        if (!empty($request->get('email'))) {
                             $instance->where(function($row) use($request){
                                $email = $request->get('email');    
                                $row->orWhere('email', 'LIKE', "%$email%");
                            });
                        }
                        if (!empty($request->get('phone'))) {
                            $instance->where(function($row) use($request){
                                $phone = $request->get('phone');    
                                $row->orWhere('phone', 'LIKE', "%$phone%");
                            });
                        }
                        if (!empty($request->get('first_name'))) {
                            $instance->where(function($row) use($request){
                                $firstName = $request->get('first_name');    
                                $row->orWhere('first_name', 'LIKE', "%$firstName%");
                            });
                        }
                        if (!empty($request->get('last_name'))) {
                            $instance->where(function($row) use($request){
                                $lastName = $request->get('last_name');    
                                $row->orWhere('last_name', 'LIKE', "%$lastName%");
                            });
                        }
                        if (!empty($request->get('gender'))) {
                            $instance->where(function($row) use($request){
                                $gender = $request->get('gender');    
                                $row->orWhere('gender', '=', "$gender");
                            });
                        }
                        if (!empty($request->get('from_date'))) {
                            $instance->where(function($row) use($request){
                                $from_date = $request->get('from_date');    
                                $row->where('birthday', '>=', "$from_date");
                            });
                        }
                        if (!empty($request->get('end_date'))) {
                            $instance->where(function($row) use($request){
                                $end_date = $request->get('end_date');    
                                $row->where('birthday', '<=', "$end_date");
                            });
                        }
                        if (!empty($request->get('role'))) {
                            $instance->whereHas('roles', function ($row) use ($request) {
                                $row->where('roles.id', $request->role);
                            });
                        }
                    })
                    ->addColumn('roles', function(User $user){
                        $roles = [];
                        foreach ($user->roles as $role) {
                            array_push($roles, $role->slug);
                        }
                        $userRole = implode(",",$roles);
                        return $userRole;
                 })->rawColumns(['roles'])->make(true);
        }
        return view('pages.users.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            $request->validate([
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'email' => 'required|email|unique:users',
                'password' => 'required|min:6',
                'password_confirmation' => 'required|same:password',
                'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
    
                $user = User::create([
                    'user_uuid' => Str::uuid()->toString(),
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'address' => $request->address,
                    'birthday' => $request->birthday,
                    'gender' => $request->gender,
                    'password' => Hash::make($request->password),
                ]);

                if ($request->hasFile('avatar')) {
                    $file = $request->file('avatar');
                    $name = time() .'_'. $file->getClientOriginalName();
                    $filePath = 'avatar/' . $name;
                    $path = Storage::disk('s3')->put($filePath, file_get_contents($file));
                    $path = Storage::disk('s3')->url( $filePath);
                    $user = User::whereId($user->id)->update([
                        'avatar' => $path
                    ]);
                }

            if($user) {
                return redirect()->route('users')->with("success", "Add user successfully!");
            } else {
                return back()->with("error", "Add user failed!");
            }
        }

        return view('pages.users.create');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $uuid)
    {
        $user = User::where('user_uuid', $uuid)->first();
        return view('pages.users.detail', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $user = User::find($id);

        if ($request->isMethod('post')) {
            if ($request->has('update-info')) {
                $request->validate([
                    'first_name' => 'required|max:255',
                    'last_name' => 'required|max:255',
                    'address' => 'max:255',
                    'email' => 'required|email',
                    'birthday' => 'date',
                    'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ]);
                $user = $user->update([
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'address' => $request->address,
                    'birthday' => $request->birthday,
                    'gender' => $request->gender,
                ]);
        
                if ($request->hasFile('avatar')) {
                    $file = $request->file('avatar');
                    $name = time() .'_'. $file->getClientOriginalName();
                    $filePath = 'avatar/' . $name;
                    $path = Storage::disk('s3')->put($filePath, file_get_contents($file));
                    $path = Storage::disk('s3')->url($filePath);
                    $user = User::whereId($id)->update([
                        'avatar' => $path
                    ]);
                }
                if($user) {
                    return redirect()->route('users')->with("success", "Update user successfully!");
                } else {
                    return back()->with("error", "Update user failed!");
                }
            }
            if ($request->has('update-password')) {
                $request->validate([
                    'password' => 'required|min:6',
                    'password_confirmation' => 'required|same:password',
                ]);
    
                $user = $user->update([
                    'password' => Hash::make($request->password),
                ]);
                if($user) {
                    return redirect()->route('users')->with("success", "Update user successfully!");
                } else {
                    return back()->with("error", "Update user failed!");
                }
            }
        }
       
        return view('pages.users.update', compact('user'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
       $user = User::find($request->user_id)->update(['delete_flg' => 1]);
       if($user) {
            return redirect()->route('users')->with("success", "Deleted user successfully!");
       } else {
            return back()->with("error", "Deleted user failed!");
       }
    }

    public function confirm(Request $request)
    {
        $user = User::find($request->user_id);
        if($user->active == 0) {
            $user->update(['active' => 1]);
        } else {
            $user->update(['active' => 0]);
        }
        if($user) {
            return redirect()->route('users')->with("success", "User activation successfully!");
        } else {
            return back()->with("error", "User activation failed!"); 
        }
    }

    public function exportPDF()
    {
        $users = User::get();
  
        $data = [
            'title' => 'Export PDF file',
            'date' => date('m/d/Y'),
            'users' => $users
        ]; 
            
        $pdf = PDF::loadView('pdf.user_list', $data);
        $pdf->setPaper('A4', 'landscape');
     
        // return $pdf->download('list-user.pdf');
        return $pdf->stream('user_list.pdf');
    }
}