<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\Auth\FacebookSocialiteController;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('admin/registration', [AuthController::class, 'registration'])->name('registration');

Route::match(['get', 'post'], 'admin/login', [AuthController::class, 'login'])->name('login');
Route::match(['get', 'post'], 'admin/sign-up', [AuthController::class, 'signup'])->name('signup');

Route::get('admin/send-mail', [AuthController::class, 'sendMail'])->name('send_mail');
Route::get('admin/verify-mail/{code}', [AuthController::class, 'verify'])->name('verify_mail');

Route::match(['get', 'post'], 'admin/send-mail-reset-password', [AuthController::class, 'sendMailResetPassword'])->name('send_mail_reset_password');
Route::match(['get', 'post'], 'admin/reset-password', [AuthController::class, 'resetPassword'])->name('reset_password');

Route::match(['get', 'post'], 'admin/reset-password/{code}', [AuthController::class, 'resetPassword'])->name('reset_password');

Route::get('auth/google', [AuthController::class, 'redirectToGoogle'])->name('auth_google');
Route::get('registration/google', [AuthController::class, 'handleCallbackGoogle']);

Route::get('auth/facebook', [AuthController::class, 'redirectToFB'])->name('auth_facebook');
Route::get('registration/facebook', [AuthController::class, 'handleCallbackFacebook']);

Route::get('logout', [AuthController::class, 'logout'])->name('logout');

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::get('admin/profile', [ProfileController::class, 'index'])->name('profile');
Route::post('admin/send-mail-change-email', [ProfileController::class, 'sendMailchangeEmail'])->name('send_mail_change_email');
Route::get('admin/change-email/{email}/{code}', [ProfileController::class, 'changeEmail'])->name('change_email');
Route::post('admin/change-password', [ProfileController::class, 'changePassword'])->name('change_password');
Route::post('admin/change-info', [ProfileController::class, 'changeInfo'])->name('change_info');


Route::get('admin/users', [UserController::class, 'index'])->name('users');
Route::match(['get', 'post'], 'admin/users/create', [UserController::class, 'create'])->name('create_user');
Route::get('admin/user/{id}', [UserController::class, 'show'])->name('detail_user');
Route::match(['get', 'post'], 'admin/users/update/{id}', [UserController::class, 'update'])->name('update_user');
Route::post('admin/user/confirm', [UserController::class, 'confirm'])->name('confirm_user');
Route::post('admin/user/delete', [UserController::class, 'destroy'])->name('delete_user');
Route::get('admin/export-user-pdf', [UserController::class, 'exportPDF'])->name('export_user_pdf');
